﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleAndScale : MonoBehaviour {
	public GameObject[] models;

	int currentModelIndex = 0;
	GameObject currentModel;
	GameObject handNode;
	bool handSetup=false;
	bool isGrabbing=false;

	void setup_model()
	{
		for (int i = 0; i < models.Length; i++)
		{
     		models [i].SetActive (false);
		}
	
		models [currentModelIndex].SetActive (true);
		currentModel = models [currentModelIndex];
		Debug.Log ("Model is now: " + currentModel.name);
	}

	void inc_model()
	{
		currentModelIndex = (currentModelIndex + 1) % models.Length;
		setup_model ();
		Debug.Log ("incrementing model");
	}

	void dec_model()
	{
		currentModelIndex = (currentModelIndex-1+models.Length) % models.Length;
		setup_model ();
		Debug.Log ("decrimenting model");
	}

	void inc_scale(float time)
	{
		Vector3 scale=currentModel.transform.localScale;
		scale = scale * (1.0f + 0.5f * time);
		currentModel.transform.localScale=scale;
	}
	void dec_scale(float time)
	{
		Vector3 scale=currentModel.transform.localScale;
		scale = scale * (1.0f - 0.5f * time);
		currentModel.transform.localScale=scale;
	}


	// Use this for initialization
	void Start () {
		setup_model ();
	}
	
	// Update is called once per frame
	void Update () {
		//print ("timeDelta: " + Time.deltaTime);
		//print ("MiddleVR : " + MiddleVR.VRKernel.GetDeltaTime ());

		if(MiddleVR.VRDeviceMgr!=null)
		{
			if (handSetup == false)
			{
				handNode = GameObject.Find ("HandNode");
				handSetup = true;
			}

			if (MiddleVR.VRDeviceMgr.IsWandButtonPressed (5))
			{
				if (isGrabbing == false)
				{
					currentModel.transform.parent = handNode.transform;
					isGrabbing = true;
				}
			} 
			else
			{
				if (isGrabbing == true)
				{
					for (int i = 0; i < models.Length; i++)
					{
						models [i].transform.parent = null; //unparent everything just to be safe
					}
					isGrabbing = false;
				}
			}




			if (MiddleVR.VRDeviceMgr.IsWandButtonToggled(0))  inc_model ();
			if (MiddleVR.VRDeviceMgr.IsWandButtonToggled(1))  dec_model ();
			if (MiddleVR.VRDeviceMgr.IsWandButtonPressed(2))  inc_scale ((float)MiddleVR.VRKernel.GetDeltaTime());
			if (MiddleVR.VRDeviceMgr.IsWandButtonPressed(3))  dec_scale ((float)MiddleVR.VRKernel.GetDeltaTime());
		}
	}
}
